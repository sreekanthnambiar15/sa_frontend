import { SocialAnalysis4Page } from './app.po';

describe('social-analysis4 App', () => {
  let page: SocialAnalysis4Page;

  beforeEach(() => {
    page = new SocialAnalysis4Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
