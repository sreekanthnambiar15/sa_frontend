import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserprofileComponent} from './layout/userprofile/userprofile.component'

const routes: Routes = [
  {
    path: '',
    loadChildren:'./layout/layout.module#LayoutModule'
  },
   {
    path:'after-auth',loadChildren:'.layout/userprofile/userprofile.module#UserprofileModule'
    
    }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
