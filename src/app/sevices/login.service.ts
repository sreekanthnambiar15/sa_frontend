import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class LoginService {

  constructor(private http:Http) { }

  signUp(data){

    return this.http.post('www.vikramreddy.org/login',data).map((res:Response)=>res.json());

  }

}
