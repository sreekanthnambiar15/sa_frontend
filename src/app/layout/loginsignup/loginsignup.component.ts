import { Component, OnInit } from '@angular/core';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {LoginService} from '../../sevices/login.service';


@Component({
  selector: 'app-loginsignup',
  templateUrl: './loginsignup.component.html',
  styleUrls: ['./loginsignup.component.css']
})

export class LoginsignupComponent implements OnInit {
  signinUsername : any;
  signinPassword : any;
  loginSuccess : any;
  

  constructor(public activeModal: NgbActiveModal,public loginService:LoginService) {
    
  }

  close(){
    this.activeModal.close();
  }

  signIn(){
    alert(this.signinUsername);
    alert(this.signinPassword);
    var data={
          email:this.signinUsername,
          password:this.signinPassword
    };
    
    // this.loginService.signUp(data).subscribe(responseData=>{

    //  // this.loginSuccess= credential.success;
      
    // },err=>{
    //   //code for error
    // });
  }

  fbLogin(){
    var width = 480,
        height = 640,
        top = (window.outerHeight - height) / 2,
        left = (window.outerWidth - width) / 2;
    var child = window.open('http://www.vikramreddy.org/login/facebook','_blank','width=' + width + ',height=' + height +
                             ',scrollbars=0,top=' + top + ',left=' + left);
    //this.userService.login();
    
    
    
     var interval = setInterval(()=>{
 try {
            console.log("child base uri is  "+child.document.baseURI); 
            console.log("parent base uri is  "+document.baseURI);
            console.log("child pathname is"+ child.document.location.pathname)
            if (
                child.document.location.pathname.search(/after-auth/) !== -1 &&
                child.document.readyState === "complete") 
                {
                  console.log("after auth reached");
                  clearInterval(interval);
              try {
                var b64_str = this.getParameterByName('data', child.document.URL);
                if (b64_str !== null) {
                  var raw_data = atob(b64_str),
                      user = JSON.parse(raw_data);
                  console.log(user);
                } else {
                  console.log("No data parameter");
                  console.log(child.document.location.pathname);
                }
              } catch (e) {
                console.log("Error authenticating user.");
                console.log(e);
              } finally {
                console.log("chil closed");
                child.close();
              }
            }
          } catch (e) {
            if (child.closed)
               clearInterval(interval);
          }

     },500); 

  }
    // -----------------------finished fb login----------------

  
  googleLogin(){
    var width = 480,
        height = 640,
        top = (window.outerHeight - height) / 2,

        left = (window.outerWidth - width) / 2;
    var child = window.open('http://www.vikramreddy.org/login/google','_blank','width=' + width + ',height=' + height +
                             ',scrollbars=0,top=' + top + ',left=' + left);
    //this.userService.login();
     var interval = setInterval(()=>{
 try {
            if (child.document.baseURI === document.baseURI &&
                child.document.location.pathname.search(/after-auth/) !== -1 &&
                child.document.readyState === "complete") 
                {
                  clearInterval(interval);
              try {
                var b64_str = this.getParameterByName('data', child.document.URL);
                if (b64_str !== null) {
                  var raw_data = atob(b64_str),
                      user = JSON.parse(raw_data);
                  
                } else {
                  console.log("No data parameter");
                  console.log(child.document.location.pathname);
                }
              } catch (e) {
                console.log("Error authenticating user.");
                console.log(e);
              } finally {
                child.close();
              }
            }
          } catch (e) {
            if (child.closed)
               clearInterval(interval);
          }

     },500); 

  }
//-------------------------------------------finished google login---------------

twitterLogin(){
    var width = 480,
        height = 640,
        top = (window.outerHeight - height) / 2,
        left = (window.outerWidth - width) / 2;
    var child = window.open(' http://www.vikramreddy.org/login/twitter','_blank','width=' + width + ',height=' + height +
                             ',scrollbars=0,top=' + top + ',left=' + left);
    //this.userService.login();
     var interval = setInterval(()=>{
 try {
            if (child.document.baseURI === document.baseURI &&
                child.document.location.pathname.search(/after-auth/) !== -1 &&
                child.document.readyState === "complete") 
                {
                  clearInterval(interval);
              try {
                var b64_str = this.getParameterByName('data', child.document.URL);
                if (b64_str !== null) {
                  var raw_data = atob(b64_str),
                      user = JSON.parse(raw_data);
                  
                } else {
                  console.log("No data parameter");
                  console.log(child.document.location.pathname);
                }
              } catch (e) {
                console.log("Error authenticating user.");
                console.log(e);
              } finally {
                child.close();
              }
            }
          } catch (e) {
            if (child.closed)
               clearInterval(interval);
          }

     },500); 

  }



// --------------------------------------------------------------------------
  getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }
  ngOnInit() {
  }


}
