import { Component, OnInit } from '@angular/core';

import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {LoginsignupComponent} from "./loginsignup/loginsignup.component"

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  constructor(private modalService: NgbModal) { }
  open() {
    const modalRef = this.modalService.open(LoginsignupComponent);
    modalRef.componentInstance.name = 'World';
  }

  

  ngOnInit() {
  }

}
