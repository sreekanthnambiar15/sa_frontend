import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent implements OnInit {

  constructor() { }
  currenttab:number=1;
  isAuthenticated:boolean;
  ngOnInit() {
  }
  changetab(tab:number){
  this.currenttab=tab;
  }

}
