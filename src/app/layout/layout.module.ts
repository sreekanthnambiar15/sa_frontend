import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgbModule, NgbModalModule,NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { LoginsignupComponent } from './loginsignup/loginsignup.component';
import {FormsModule} from '@angular/forms'

import { LayoutRoutingModule } from './layout-routing.module';
import {LayoutComponent} from './layout.component';
import { UserprofileComponent } from './userprofile/userprofile.component';

@NgModule({
  imports: [
    CommonModule,
    LayoutRoutingModule,
    NgbModule.forRoot(),
    FormsModule
  ],
  providers: [NgbActiveModal],entryComponents:[LoginsignupComponent],
  declarations: [LayoutComponent,LoginsignupComponent]
})
export class LayoutModule { }
